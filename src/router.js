import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Signup from './views/Signup.vue'

// Store
import store from './store'

// Socket auth
import { authorize } from './socket/auth'

/**
 * A function for all routes that requires user to be authorized
 * @param {Object} to
 * @param {Object} from
 * @param {Function} next
 */
async function authRequired (to, from, next) {
  let isAuthorized = store.getters['authModule/getIsAuthorized']
  if (!isAuthorized) {
    await store.dispatch('authModule/verify')
    isAuthorized = store.getters['authModule/getIsAuthorized']
    if (isAuthorized) {
      authorize()
      next()
    } else {
      next('/login')
    }
  } else {
    authorize()
    next()
  }
}

/**
 * A function for all routes that requires user to be not be authorized
 * @param {Object} to
 * @param {Object} from
 * @param {Function} next
 */
async function negativeAuthRequired (to, from, next) {
  let isAuthorized = store.getters['authModule/getIsAuthorized']
  if (!isAuthorized) {
    await store.dispatch('authModule/verify')
    isAuthorized = store.getters['authModule/getIsAuthorized']
    if (isAuthorized) {
      next('/')
    } else {
      next()
    }
  } else {
    next('/')
  }
}

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: authRequired
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: negativeAuthRequired
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
      beforeEnter: negativeAuthRequired
    }
  ]
})
