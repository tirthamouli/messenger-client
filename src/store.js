import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import authModule from '@/store/auth/authModule'
import friendsModule from '@/store/friends/friendsModule'
import messagesModule from '@/store/message/messagesModule'
import videoModule from '@/store/video/videoModule.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    authModule,
    friendsModule,
    messagesModule,
    videoModule
  }
})
