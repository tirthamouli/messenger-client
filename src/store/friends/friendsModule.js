import Vue from 'vue'

// Function to make request where auth token is added in the header by default
import { authRequest } from '../common/authRequest'

// Timeout for when sending a request should be sent
let searchFriendsTimeout = false

const state = {
  searchResults: [], // List of all the search results
  loadingSearch: 0, // If any search result is being brought in this is incremented. If it is 0, then loading is completed
  sendingToIds: [], // Id where request is currently being sent
  acceptOrReject: {}, // If currently request is being accepted or rejected
  friends: [], // Stores all the friends of the user
  selectedFriendIndex: 0, // Currently selected index of the firend
  loadingFriends: 0, // If friends are being loaded, this is increased and decreased once done
  notification: {
    message: '',
    id: ''
  }, // Notification when new message is received,
  showNotification: {} // Whose notification show be shown. If id is not in the object show notification for that user
}

const getters = {
  getSearchResults (state) {
    return state.searchResults
  },
  getLoadingSearch (state) {
    return state.loadingSearch
  },
  getSendingToIds (state) {
    return state.sendingToIds
  },
  getLoadingFriends (state) {
    return state.loadingFriends
  },
  getFriends (state) {
    return state.friends
  },
  getAcceptOrReject (state) {
    return state.acceptOrReject
  },
  getSelectedFriendIndex (state) {
    return state.selectedFriendIndex
  },
  getNotification (state) {
    return state.notification
  },
  getShowNotification (state) {
    return state.showNotification
  },
  getCurrentSelectedFriend (state) {
    if (state.selectedFriendIndex < state.friends.length) {
      return state.friends[state.selectedFriendIndex]
    } else {
      return false
    }
  }
}

const mutations = {
  increaseLoadingCount (state) {
    state.loadingSearch++
  },
  decreaseLoadingCount (state) {
    state.loadingSearch--
  },
  increaseLoadingFriends (state) {
    state.loadingFriends++
  },
  decreaseLoadingFriends (state) {
    state.loadingFriends--
  },
  addSendingToId (state, value) {
    state.sendingToIds = [...state.sendingToIds, value]
  },
  acceptingRequest (state, id) {
    Vue.set(state.acceptOrReject, id, 'accept')
  },
  rejectingRequest (state, id) {
    Vue.set(state.acceptOrReject, id, 'reject')
  },
  removeFromSendingToId (state, value) {
    const index = state.sendingToIds.indexOf(value)
    if (index !== -1) {
      state.sendingToIds.splice(index, 1)
    }
  },
  initialFriendsSet (state, friends) {
    state.friends = friends
  },
  setSelectedFriendIndex (state, index) {
    if (typeof index === 'number' && state.friends.length > index) {
      state.selectedFriendIndex = index
    }
  },
  setSelectedFriendId (state, id) {
    // Step 1: Get index from id
    const index = state.friends.findIndex(friend => {
      return friend.friendDetails._id === id
    })

    // Step 2: Set the index
    if (index !== -1) {
      state.selectedFriendIndex = index
    }
  },
  addFriend (state, friend) {
    if (state.friends.length !== 0) {
      state.selectedFriendIndex++
    }
    state.friends = [friend, ...state.friends]
  },
  setNotification (state, { id, message }) {
    state.notification = {
      id, message
    }
  },
  addShowNotification (state, id) {
    Vue.set(state.showNotification, id, false)
  },
  removeShowNotification (state, id) {
    Vue.set(state.showNotification, id, true)
  },
  requestSentSuccess (state, id) {
    state.searchResults = state.searchResults.map(user => {
      if (user._id === id) {
        user.type = 'wait'
      }
      return { ...user }
    })
  },
  requestAcceptSuccess (state, id) {
    state.searchResults = state.searchResults.map(user => {
      if (user._id === id) {
        user.type = 'friend'
      }
      return { ...user }
    })
  },
  addToSearchList (state, users) {
    // Step 1: Check if users is an array and length is more than 0
    if (users.constructor === Array && users.length > 0) {
      let ids = []
      // Step 2: Concat the old array and new array, new array first to remove old data and keep the new one
      const newUsersList = [...users, ...state.searchResults]

      // Step 3: Filter the duplicate ids
      state.searchResults = newUsersList.filter(user => {
        if (ids.indexOf(user._id) === -1) {
          ids.push(user._id)
          return true
        } else {
          return false
        }
      })
    }
  },
  /**
   * Increase the unread message by 1
   * @param {Object} state
   * @param {String} id
   */
  increaseUnreadMessages (state, id) {
    // Step 1: Increase unread of friend
    const newFriends = state.friends.map(friend => {
      if (friend.friendDetails._id === id) {
        friend.unreadMessages++
        friend.new = false
      }
      return friend
    })

    // Set the state
    state.friends = newFriends
  },
  /**
   *
   * @param {Object} state
   * @param {String} id
   */
  removeUnread (state, id) {
    // Step 1: Remove unread of friend
    const newFriends = state.friends.map(friend => {
      if (friend.friendDetails._id === id) {
        friend.unreadMessages = 0
      }
      return friend
    })

    // Set the state
    state.friends = newFriends
  },
  reorderFriends (state, { id, time }) {
    // Step 1: Update last activity time of friend
    let newFriends = state.friends.map(friend => {
      if (friend.friendDetails._id === id) {
        friend.lastActionTime = time
        friend.new = false
      }
      return friend
    })

    // Step 2: Reorder
    newFriends = newFriends.sort((x, y) => {
      return x.lastActionTime > y.lastActionTime ? -1 : 1
    })

    // Step 3: Getting the selected index correctly
    let newSelectedIndex = false
    if (state.selectedFriendIndex < state.friends.length) {
      const selectedFriendId = state.friends[state.selectedFriendIndex].friendDetails._id
      newSelectedIndex = newFriends.findIndex(friend => {
        return friend.friendDetails._id === selectedFriendId
      })
    }

    // Step 4: Set the state
    state.friends = newFriends

    // Step 5: Setting the selected index correctly
    if (newSelectedIndex !== false) {
      state.selectedFriendIndex = newSelectedIndex
    }
  },
  /**
   * Clear state when logging out
   * @param {Object} state
   */
  clear (state) {
    state.searchResults = []
    state.loadingSearch = 0
    state.sendingToIds = []
    state.acceptOrReject = {}
    state.friends = []
    state.selectedFriendIndex = 0
    state.loadingFriends = 0
    state.notification = {
      message: '',
      id: ''
    }
    state.showNotification = {}
  }
}

const actions = {
  /**
   * Search for user
   * @param {Object} context
   * @param {String} value
   */
  searchFriend (context, value) {
    value = value.trim().length > 0 ? value.trim() : false
    if (value) {
      // Using timeout to prevent sending request with each key press
      if (searchFriendsTimeout) {
        clearTimeout(searchFriendsTimeout)
      }
      searchFriendsTimeout = setTimeout(async () => {
        try {
          // Incresing the loading count
          context.commit('increaseLoadingCount')

          const json = await authRequest({
            method: 'GET',
            body: false,
            url: 'friend/search_friend/' + encodeURIComponent(value)
          })

          if ('users' in json) {
            context.commit('addToSearchList', json.users)
          }

          // Decreasing the loading count
          context.commit('decreaseLoadingCount')
        } catch (err) {
          context.dispatch('authModule/logout', null, { root: true })
        }
      }, 500)
    }
  },
  /**
   * Send a friend request to another user
   * @param {Object} context
   * @param {String} id
   */
  async sendRequest (context, id) {
    id = id.trim().length > 0 ? id.trim() : false
    if (id) {
      try {
        // Loading the current id
        context.commit('addSendingToId', id)

        // Send friend request
        const json = await authRequest({
          method: 'POST',
          body: JSON.stringify({
            id
          }),
          url: 'friend/send_request'
        })

        // Request sent successfully
        if ('success' in json && json.success) {
          context.commit('requestSentSuccess', id)
        }

        // Stop loading
        context.commit('removeFromSendingToId', id)
      } catch (err) {
        context.dispatch('authModule/logout', null, { root: true })
      }
    }
  },
  /**
   *
   * @param {Object} context
   * @param {String} id
   */
  async acceptFriendRequest (context, id) {
    id = id.trim().length > 0 ? id.trim() : false
    if (id) {
      try {
        // Loading the current id
        context.commit('acceptingRequest', id)
        context.commit('addSendingToId', id)

        // Send friend request
        const json = await authRequest({
          method: 'POST',
          body: JSON.stringify({
            id
          }),
          url: 'friend/accept_request'
        })

        // Request sent successfully
        if ('success' in json && json.success) {
          context.commit('requestAcceptSuccess', id)

          const newFriendDetail = context.getters.getSearchResults.find(user => {
            return user._id === id
          })

          const newFriend = {
            friendDetails: newFriendDetail,
            lastActionTime: Date.now(),
            lastActionType: 3,
            new: true
          }

          context.commit('addFriend', newFriend)
        }

        // Stop loading
        context.commit('removeFromSendingToId', id)
      } catch (err) {
        context.dispatch('authModule/logout', null, { root: true })
      }
    }
  },
  /**
   * Get all the pending request of the user
   * @param {Object} context
   */
  async getPendingRequests (context) {
    try {
      // Incresing the loading count
      context.commit('increaseLoadingCount')

      // Fetch all the friend request
      const json = await authRequest({
        method: 'GET',
        body: false,
        url: 'friend/get_pending_requests/'
      })

      // If friend request is there in json, add it to search list after formatting it
      if ('friendRequests' in json) {
        const friendRequests = json.friendRequests.map(user => {
          return { ...user, type: 'waiting' }
        })
        context.commit('addToSearchList', friendRequests)
      }

      // Decreasing the loading count
      context.commit('decreaseLoadingCount')
    } catch (err) {
      context.dispatch('authModule/logout', null, { root: true })
    }
  },
  /**
   * Get all the friends of the user
   * @param {Object} context
   */
  async getFriends (context) {
    try {
      // Incresing the loading count
      context.commit('increaseLoadingFriends')

      // Fetch all the friend request
      const json = await authRequest({
        method: 'GET',
        body: false,
        url: 'friend/get_friends/'
      })

      // If friend request is there in json, add it to search list after formatting it
      if ('friends' in json) {
        // User id of the current logged in user
        const userID = context.rootGetters['authModule/getUserId']

        const formattedFriends = json.friends.map(friend => {
          // Extracting details about the friend
          const friendDetails = friend.friendOne._id === userID
            ? friend.friendTwo : friend.friendOne

          // Unread message of that user
          const unreadMessages = friend.friendOne._id !== userID
            ? friend.friendTwoUnread : friend.friendOneUnread

          return {
            friendDetails,
            unreadMessages,
            new: friend.new,
            lastActionTime: friend.lastActionTime,
            lastActionType: friend.lastActionType
          }
        })

        context.commit('initialFriendsSet', formattedFriends)
      }

      // Decreasing the loading count
      context.commit('decreaseLoadingFriends')
    } catch (err) {
      context.dispatch('authModule/logout', null, { root: true })
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
