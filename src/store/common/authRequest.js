import { config } from '../config/config'

export const authRequest = ({ method, body, url }) => {
  return new Promise(async (resolve, reject) => {
    try {
      // Step 1: Getting token from local storage
      let token = localStorage.getItem('auth')
      if (token !== null) {
        // Step 2: Convert string json to object json
        token = JSON.parse(token)

        // Step 3: Formatting the request message
        let requestObject = {
          method: method, // GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, cors, *same-origin
          cache: 'no-cache', // default, no-cache, reload, force-cache, only-if-cached
          credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'id': token.id,
            'token': token.token,
            'tokenid': token.tokenId
          },
          redirect: 'follow', // manual, *follow, error
          referrer: 'no-referrer' // no-referrer, *client
        }

        // Step 4: Adding a body for post method
        if (method === 'POST' || method === 'PUT' || method === 'DELETE') {
          requestObject.body = body
        }

        // Step 5: Sending request
        const response = await fetch(config.BASE_URL + url, requestObject)

        // Step 6: Addition requirement in fetch API
        const json = await response.json()

        // Step 7: Checking if any error is found and resolve it accordingly
        if ('code' in json && json.code !== 200) {
          reject(new Error(json))
        } else if ('code' in json && json.code === 200) {
          json.authToken = token // Sending auth token as well
          resolve(json)
        } else {
          reject(new Error('unexpected'))
        }
      } else {
        reject(new Error('unauthorized')) // No token was found
      }
    } catch {
      reject(new Error('unexpected')) // Something went wrong
    }
  })
}
