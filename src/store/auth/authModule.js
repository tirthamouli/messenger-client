import Vue from 'vue'
import { config } from '../config/config'
import { authRequest } from '../common/authRequest'

/**
 * General function for makig a post request
 * @param {String} requestData
 */
function postRequest (requestData, url) {
  // Step 1 : Return promise
  return new Promise(async (resolve, reject) => {
    try {
      // Step 2: Send post request
      const res = await fetch(config.BASE_URL + url, {
        method: 'POST', // GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: requestData
      })

      // Step 3: Addition step for fetch API
      const json = await res.json()

      // Resolve the json
      resolve(json)
    } catch (e) {
      // Do nothing
    }
  })
}

const state = {
  verified: false, // To check if user has gone through the verification process
  loading: false, // Whenever some thing is being fetched or posted from the server, this is set to true
  errorMessage: '', // If any error message is sent form the server this is set to true
  name: {
    firstName: '', // First name of the user
    lastName: '' // Last name of the user
  },
  authToken: {
    id: '', // User id
    tokenId: '', // Token id
    token: '' // Token value
  },
  socketAuthorized: false // Check if user is verified over socket connection
}

const getters = {
  getIsAuthorized (state) {
    return state.authToken.id !== '' && state.tokenId !== '' && state.token !== ''
  },
  getUserId (state) {
    return state.authToken.id
  },
  getLoading (state) {
    return state.loading
  },
  getErrorMessage (state) {
    return state.errorMessage !== '' ? state.errorMessage : false
  },
  getIsVerified (state) {
    return state.verified
  },
  getName (state) {
    return state.name
  },
  getAuthToken (state) {
    return state.authToken
  },
  getSocketAuthorized (state) {
    return state.socketAuthorized
  }
}

const mutations = {
  /**
   *
   * @param {Object} state
   * @param {Boolean} value
   */
  setLoading (state, value) {
    state.loading = value
  },
  /**
   * Set weather the user has been authorized over socket connection
   * @param {Object} state
   * @param {Boolean} value
   */
  setSocketAuthorized (state, value) {
    state.socketAuthorized = value
  },
  /**
   *
   * @param {Object} state
   * @param {Object} token
   */
  setAuthTokenFromLogin (state, token) {
    // Step 1: Type checking
    if (typeof token === 'object' && 'id' in token && 'token' in token && 'tokenId' in token) {
      // Step 2: Trimming the response
      const id = typeof token.id === 'string' && token.id.trim().length > 0 ? token.id.trim() : false
      const tokenId = typeof token.tokenId === 'string' && token.tokenId.trim().length > 0 ? token.tokenId.trim() : false
      const tokenString = typeof token.token === 'string' && token.token.trim().length > 0 ? token.token.trim() : false

      // If data is properly formatted insert it into the state and local storage
      if (id && tokenId && tokenString) {
        const authToken = {
          id: id,
          tokenId: tokenId,
          token: tokenString
        }

        // Setting local storage
        window.localStorage.setItem('auth', JSON.stringify(authToken))

        // Setting auth token in state
        state.authToken = authToken
        return true
      }
    }

    // Set an error message if some wrong data is returned from the server
    state.errorMessage = 'something went wrong'
  },
  /**
   * Setting auth token after verification
   * @param {Object} state
   * @param {Object} token
   */
  setAuthTokenFromVerify (state, token) {
    state.authToken = token
  },
  /**
   * Setting first name and last name of the logged in user
   * @param {Object} param0
   */
  setName (state, { firstName, lastName }) {
    if (typeof firstName === 'string' && typeof lastName === 'string') {
      Vue.set(state.name, 'firstName', firstName)
      Vue.set(state.name, 'lastName', lastName)
    }
  },
  /**
   * Setting the value of is verified
   * @param {Object} state
   * @param {Boolean} value
   */
  setVerified (state, value) {
    state.verified = value
  },
  /**
   *
   * @param {Object} state
   * @param {String} message
   */
  setErrorMessage (state, message) {
    if (typeof message === 'string') {
      state.errorMessage = message.trim()
    }
  },
  /**
   * Clears the state when logged out
   * @param {Object} state
   */
  clear (state) {
    state.loading = false
    state.errorMessage = ''
    state.name = {
      firstName: '',
      lastName: ''
    }
    state.authToken = {
      id: '',
      tokenId: '',
      token: ''
    }
    state.socketAuthorized = false

    // Clearing local storage
    window.localStorage.clear('auth')
  }
}

const actions = {
  /**
   *
   * @param {Object} context
   * @param {String} requestData
   */
  async login (context, requestData) {
    try {
      // Setting loading to true and emptying error message
      context.commit('setErrorMessage', '')
      context.commit('setLoading', true)

      // Sending post request
      const json = await postRequest(requestData, 'auth/login')

      // Check if user has successfully logged in or if error message was sent
      if ('code' in json && json.code === 200 && 'token' in json) {
        // Setting auth token
        context.commit('setAuthTokenFromLogin', json.token)
        // Setting name
        context.commit('setName', { firstName: json.token.firstName, lastName: json.token.lastName })
      } else if ('code' in json && json.code === 403 && 'error' in json) {
        // Setting error message
        context.commit('setErrorMessage', json.error)
      } else {
        // Setting default error message
        context.commit('setErrorMessage', 'enter proper username or password')
      }

      // Setting loading to false
      context.commit('setLoading', false)
    } catch (e) {
      // Do nothing
    }
  },
  /**
   *
   * @param {Object} context
   * @param {String} requestData
   */
  async signup (context, requestData) {
    try {
      // Setting loading to true and emptying error message
      context.commit('setErrorMessage', '')
      context.commit('setLoading', true)

      const json = await postRequest(requestData, 'auth/signup')

      // Check if user has successfully logged in or if error message was sent
      if ('code' in json && json.code === 200 && 'token' in json) {
        // Setting auth token
        context.commit('setAuthTokenFromLogin', json.token)
        // Setting name
        context.commit('setName', { firstName: json.token.firstName, lastName: json.token.lastName })
      } else if ('code' in json && json.code === 403 && 'error' in json) {
        // Setting error message
        context.commit('setErrorMessage', json.error)
      } else {
        // Setting default error message
        context.commit('setErrorMessage', 'enter proper first name, last name, username or password')
      }

      // Setting loading to false
      context.commit('setLoading', false)
    } catch (e) {
      // Do nothing
    }
  },
  /**
   * Verify if user is authorized
   * @param {Object} context
   */
  async verify (context) {
    try {
      const json = await authRequest({
        method: 'GET',
        body: false,
        url: 'auth/verify'
      })

      const authToken = json.authToken
      const firstName = json.firstName
      const lastName = json.lastName

      // Setting the auth token
      context.commit('setAuthTokenFromVerify', authToken)

      // Setting name
      context.commit('setName', { firstName, lastName })

      // Setting verified
      context.commit('setVerified', true)
    } catch (e) {
      // Setting verified
      context.commit('setVerified', true)
      context.dispatch('logout') // Log user out
    }
  },
  /**
   * Log user out
   * @param {Object} context
   */
  logout (context) {
    // Step 1: Log user out
    authRequest({
      method: 'GET',
      body: false,
      url: 'auth/logout'
    }).then(() => {
      // Do nothing
    }).catch(() => {
      // Do nothing
    })

    // Step 2: Clear all the modules
    context.commit('clear') // Auth
    context.commit('friendsModule/clear', null, { root: true }) // Friends
    context.commit('messagesModule/clear', null, { root: true }) // Messages
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
