import Vue from 'vue'

// Function to make request where auth token is added in the header by default
import { authRequest } from '../common/authRequest'

const state = {
  messages: {},
  pages: {},
  additionalOffset: {},
  lastPage: {},
  loadingMessage: {},
  sendingMessage: {},
  loadedMessagesOfUsers: []
}

const getters = {
  getMessages (state) {
    return state.messages
  },
  getPages (state) {
    return state.pages
  },
  getLastPage (state) {
    return state.lastPage
  },
  getAdditionalOffset (state) {
    return state.additionalOffset
  },
  getLoadingMessage (state) {
    return state.loadingMessage
  },
  getLoadedMessagesOfUsers (state) {
    return state.loadedMessagesOfUsers
  }
}

const mutations = {
  startLoadingMessage (state, id) {
    Vue.set(state.loadingMessage, id, true)
  },
  stopLoadingMessage (state, id) {
    Vue.set(state.loadingMessage, id, false)
  },
  startSendingMessage (state, id) {
    Vue.set(state.sendingMessage, id, true)
  },
  stopSendingMessage (state, id) {
    Vue.set(state.sendingMessage, id, false)
  },
  setPage (state, { id, pageNumber }) {
    Vue.set(state.pages, id, pageNumber)
  },
  setLastPage (state, id) {
    Vue.set(state.lastPage, id, true)
  },
  prependMessages (state, { messages, id }) {
    if (id in state.messages) {
      Vue.set(state.messages, id, [...messages, ...state.messages[id]])
    } else {
      Vue.set(state.messages, id, messages)
    }
  },
  addMessageFromClient (state, { id, message }) {
    if (id in state.messages) {
      // Adding the message
      Vue.set(state.messages, id, [...state.messages[id], message])

      // Increase the additional page offset
      if (id in state.additionalOffset) {
        Vue.set(state.additionalOffset, id, state.additionalOffset[id] + 1)
      } else {
        Vue.set(state.additionalOffset, id, 1)
      }
    } else {
      Vue.set(state.messages, id, [message])
    }
  },
  updateAddedMessage (state, { messageId, index, friendId }) {
    const messages = state.messages[friendId]
    messages[index]._id = messageId
    Vue.set(state.messages, friendId, messages)
  },
  addMessageFromServer (state, { id, message }) {
    if (id in state.messages && state.loadedMessagesOfUsers.indexOf(id) !== -1) {
      // Adding the message
      Vue.set(state.messages, id, [...state.messages[id], message])

      // Increase the additional page offset
      if (id in state.additionalOffset) {
        Vue.set(state.additionalOffset, id, state.additionalOffset[id] + 1)
      } else {
        Vue.set(state.additionalOffset, id, 1)
      }
    } else if (state.loadedMessagesOfUsers.indexOf(id) !== -1) {
      Vue.set(state.messages, id, [message])
    }
  },
  addLoadedMessagesOfUsers (state, id) {
    if (state.loadedMessagesOfUsers.indexOf(id) === -1) {
      state.loadedMessagesOfUsers.push(id)
    }
  },
  removeLoadedMessagesOfUsers (state, id) {
    const index = state.loadedMessagesOfUsers.indexOf(id)
    if (index !== -1) {
      const newLoaded = [...state.loadedMessagesOfUsers]
      newLoaded.splice(index, 1)
      state.loadedMessagesOfUsers = newLoaded
    }
  },
  /**
   * Clear state when logging out
   * @param {Object} state
   */
  clear (state) {
    state.messages = {}
    state.pages = {}
    state.additionalOffset = {}
    state.lastPage = {}
    state.loadingMessage = {}
    state.sendingMessage = {}
    state.loadedMessagesOfUsers = []
  }
}

let clientGeneratedMessageId = 0

const actions = {
  /**
   * Send text message to friend
   * @param {Object} context
   * @param {String} message
   */
  async sendMessage (context, message) {
    const currentSelectedFriend = context.rootGetters['friendsModule/getCurrentSelectedFriend']
    if (currentSelectedFriend) {
      const friendId = currentSelectedFriend.friendDetails._id
      const userId = context.rootGetters['authModule/getUserId']

      // Start loading message
      context.commit('startSendingMessage', friendId)

      try {
        // Generate a message on the client side and add it
        //  to the array of message for quickly displaying a message and later editing the id
        const currentClientGeneratedMessageId = clientGeneratedMessageId++
        const newMessageLocal = {
          From: userId,
          To: friendId,
          _id: currentClientGeneratedMessageId.toString(),
          content: message,
          status: 'not-read',
          timeSent: Date.now(),
          type: 'text',
          animate: false
        }

        // Getting the index at which the message is going to be inserted, so that later it can be edited with ease
        const messages = context.getters.getMessages
        let index = 0
        if (friendId in messages) {
          index = messages[friendId].length
        }

        // Adding message to list of message
        context.commit('addMessageFromClient', {
          message: newMessageLocal,
          id: friendId
        })

        // Send the message
        const newMessage = await authRequest({
          method: 'POST',
          body: JSON.stringify({
            id: friendId,
            message: message
          }),
          url: 'message/send_message/text'
        })

        // Updating the added message
        if ('newMessage' in newMessage) {
          // Message has been successfully sent and editing the id
          context.commit('updateAddedMessage', { messageId: newMessage.newMessage._id, index: index, friendId })

          // Reordering friends list
          context.commit('friendsModule/reorderFriends', {
            id: friendId,
            time: newMessage.newMessage.timeSent
          }, {
            root: true
          })
        }
      } catch (err) {
        context.dispatch('authModule/logout', null, { root: true })
      }

      // Stop loading message
      context.commit('stopSendingMessage', friendId)
    }
  },
  /**
   * Get the messages of the current selected user
   * @param {Object} context
   */
  async getMessages (context, nextPage = false) {
    const currentSelectedFriend = context.rootGetters['friendsModule/getCurrentSelectedFriend']
    const loadedMessagesOfUsers = context.getters.getLoadedMessagesOfUsers
    let friendId = -1
    if (currentSelectedFriend) {
      friendId = currentSelectedFriend.friendDetails._id
    }
    if (friendId !== -1 && (
      (
        currentSelectedFriend && loadedMessagesOfUsers.indexOf(friendId) === -1) ||
        nextPage
    )
    ) {
      // Start loading message
      context.commit('startLoadingMessage', friendId)

      // Already loaded details
      context.commit('addLoadedMessagesOfUsers', friendId)

      // Check if we have reached last page for this friend
      const lastPage = friendId in context.getters.getLastPage
        ? context.getters.getLastPage[friendId] : false

      if (!lastPage) {
        // Get current page that needs to be fetched
        const currentPage = friendId in context.getters.getPages
          ? context.getters.getPages[friendId] + 1 : 0

        const additionalOffset = friendId in context.getters.getAdditionalOffset
          ? context.getters.getAdditionalOffset[friendId] : 0

        try {
          // Get all messages
          const json = await authRequest({
            method: 'GET',
            body: false,
            url: `message/get_message/${friendId}/${currentPage}/${additionalOffset}`
          })

          if ('messages' in json) {
            if (json.messages.length < 30) {
              // Last page reached
              context.commit('setLastPage', friendId)
            }

            if (json.messages.length > 0) {
              // Prepend to message
              json.messages.reverse()
              context.commit('prependMessages', { messages: json.messages, id: friendId })
            }
          }

          // Increase page count
          context.commit('setPage', { id: friendId, pageNumber: currentPage })
        } catch (err) {
          context.dispatch('authModule/logout', null, { root: true })

          // Remove from loaded list since there was an error
          context.commit('removeLoadedMessagesOfUsers', friendId)
        }
      }

      // Stop loading message
      context.commit('stopLoadingMessage', friendId)
    }
  },
  /**
   * Read message
   * @param {Object} context
   */
  readMessage (context) {
    // Step 1: Get the current selected friend
    const currentSelectedFriend = context.rootGetters['friendsModule/getCurrentSelectedFriend']
    if (currentSelectedFriend && currentSelectedFriend.unreadMessages > 0) {
      // Step 2: Get the id of the currently selected friend
      const friendId = currentSelectedFriend.friendDetails._id
      try {
        // Read the message
        authRequest({
          method: 'PUT',
          body: JSON.stringify({
            id: friendId
          }),
          url: 'message/read_message'
        })

        // Set unread as 0
        context.commit('friendsModule/removeUnread', friendId, {
          root: true
        })
      } catch (err) {
        context.dispatch('authModule/logout', null, {
          root: true
        })
      }
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
