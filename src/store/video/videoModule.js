// Function to make request where auth token is added in the header by default
import { authRequest } from '../common/authRequest'

const state = {
  receivingCall: false,
  senderID: false,
  senderConnection: false
}

const getters = {
  getReceivingCall (state) {
    return state.receivingCall
  },
  getSenderID (state) {
    return state.senderID
  },
  getSenderConnection (state) {
    return state.senderConnection
  }
}

const mutations = {
  setReceivingCall (state, value = false) {
    state.receivingCall = value
  },
  setSenderID (state, value = false) {
    state.senderID = value
  },
  setSenderConnection (state, value = false) {
    try {
      state.senderConnection = JSON.parse(value)
    } catch {
      state.senderConnection = false
      state.senderID = false
      state.receivingCall = false
    }
  },
  clear (state) {
    state.senderConnection = false
    state.senderID = false
    state.receivingCall = false
  }
}

const actions = {
  /**
   * Send video call request
   * @param {Object} context
   * @param {Object} connection
   */
  async sendVideoCallRequest (context, { connection, initiator }) {
    const currentSelectedFriend = context.rootGetters['friendsModule/getCurrentSelectedFriend']
    if (currentSelectedFriend) {
      const friendId = currentSelectedFriend.friendDetails._id

      try {
        const videoCall = await authRequest({
          method: 'POST',
          body: JSON.stringify({
            id: friendId,
            connection: JSON.stringify(connection),
            accept: !initiator
          }),
          url: 'video/call_request'
        })

        // Updating the added message
        if ('success' in videoCall && !videoCall.success) {
          // Create error message
        }
      } catch (err) {
        context.dispatch('authModule/logout', null, { root: true })
      }
    }
  },
  /**
   * End the current on going video call
   * @param {Object} context
   */
  async stopVideoCall (context) {
    const currentSelectedFriend = context.rootGetters['friendsModule/getCurrentSelectedFriend']
    if (currentSelectedFriend) {
      const friendId = currentSelectedFriend.friendDetails._id

      try {
        // Send the video call request
        const videoCall = await authRequest({
          method: 'DELETE',
          body: JSON.stringify({
            id: friendId
          }),
          url: 'video/end_call'
        })

        // Updating the added message
        if ('success' in videoCall && !videoCall.success) {
          // Create error message
        }
      } catch (err) {
        context.dispatch('authModule/logout', null, { root: true })
      }
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
