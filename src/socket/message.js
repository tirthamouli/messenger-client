import store from '../store'

export const message = socket => {
  socket.on('messageReceived', message => {
    const userId = store.getters['authModule/getUserId']
    if (message.From === userId) {
      // Add the message to the list
      store.commit('messagesModule/addMessageFromServer', {
        id: message.To,
        message: message
      })

      // Reorder the friend list according to the newest activity
      store.commit('friendsModule/reorderFriends', { id: message.To, time: message.timeSent })
    } else {
      // Add the message to the list
      store.commit('messagesModule/addMessageFromServer', {
        id: message.From,
        message: message
      })

      // Increase the unread message by 1
      store.commit('friendsModule/increaseUnreadMessages', message.From)

      // Reorder the friend list according to the newest activity
      store.commit('friendsModule/reorderFriends', { id: message.From, time: message.timeSent })

      // Send notification
      const showNotification = store.getters['friendsModule/getShowNotification']
      const currentSelectedFriend = store.getters['friendsModule/getCurrentSelectedFriend']
      if ((!(message.From in showNotification) || !showNotification[message.From]) && currentSelectedFriend.friendDetails._id !== message.From) {
        // Get all the friends
        const friends = store.getters['friendsModule/getFriends']

        // Get the name of the current friend
        const friend = friends.find(currFriend => {
          return currFriend.friendDetails._id === message.From
        })

        // Name of the friend
        const name = friend.friendDetails.firstName + ' ' + friend.friendDetails.lastName

        // Set notification
        store.commit('friendsModule/setNotification', { id: message.From, message: name + ' sent a message' })

        // Don't show notification for that person again
        store.commit('friendsModule/removeShowNotification', message.From)
      }
    }
  })
}
