import socket from '../socket'
import store from '../store'

/**
 * Authorize the user over the socket connection as well
 */
export const authorize = () => {
  // Step 1: Check if user has already been authorized
  const isAuthorized = store.getters['authModule/getSocketAuthorized']

  if (!isAuthorized) {
    // Step 2: If not authorized get the auth token
    const authToken = store.getters['authModule/getAuthToken']

    // Step 3: Send auth request
    socket.emit('authorize', authToken)
  }

  // Set that the user has been authorized
  socket.on('authorized', () => {
    console.log('authorized')
    store.commit('authModule/setSocketAuthorized', true)
  })
}
