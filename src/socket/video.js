// messageReceived
import store from '../store'

export const request = socket => {
  socket.on('videoCallRequest', ({ from, connection }) => {
    // Step 1: Check if user is already in a call
    const receivingCall = store.getters['videoModule/getReceivingCall']
    if (!receivingCall) {
      store.commit('videoModule/setReceivingCall', true)
      store.commit('videoModule/setSenderID', from)
      store.commit('videoModule/setSenderConnection', connection)
    } else {
      // Send user busy response
    }
  })
}

export const accept = socket => {
  socket.on('callAccept', ({ from, connection }) => {
    // Step 1: Check if user is already in a call
    const receivingCall = store.getters['videoModule/getReceivingCall']
    if (!receivingCall) {
      store.commit('videoModule/setSenderID', from)
      store.commit('videoModule/setSenderConnection', connection)
    } else {
      // Send user busy response
    }
  })
}
