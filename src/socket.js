import io from 'socket.io-client'
import { config } from './store/config/config'
import store from './store'
import { message } from './socket/message'
import { request, accept } from './socket/video'

const socket = io(config.BASE_URL)

/**
 * Listening for when connection has been established
 */
socket.on('connect', () => {
  console.log('Websocket connected')
  message(socket)
  request(socket)
  accept(socket)
})

/**
 * Logout user when 403 has been sent from socket
 */
socket.on(403, () => {
  store.dispatch('authModule/logout', null, { root: true })
})

export default socket
