import { mapGetters } from 'vuex'

/**
 * Mixin to change to login if authorization is lost or switch to dashboard if authorization is gained
 */
export const authMixin = {
  computed: {
    ...mapGetters('authModule', {
      getIsAuthorized: 'getIsAuthorized' // Check if user is authorized
    })
  },
  watch: {
    /**
     * Watching the authorization status for any chnage and if there is any change switching the page
     * @param {Boolean} value
     */
    getIsAuthorized (value) {
      if (value) {
        this.$router.push('/')
      } else {
        this.$router.push('/login')
      }
    }
  }
}
